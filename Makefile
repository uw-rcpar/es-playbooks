run:
	ansible-playbook logserver.yml --vault-password-file ~/.vault_pass.txt --extra-vars "esinit=false"

init:
	ansible-playbook logserver.yml --vault-password-file ~/.vault_pass.txt --extra-vars "esinit=true"

push:
	rsync -avs . rcpar:/home/ubuntu/es-playbooks/
