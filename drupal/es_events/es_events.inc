<?php
/**
 * @file
 * ES Events connection file module.
 */

use Elasticsearch\ClientBuilder;

/**
 * Returns ES client.
 */
function es_events_es_client() {
  // Return client if already loaded.
  static $es_events_es_client;
  if (isset($es_events_es_client)) {
    return $es_events_es_client;
  }

  // Load client
  $es_hosts = variable_get('es_events_es_hosts');
  if (!is_array($es_hosts) || count($es_hosts) === 0) {
    return FALSE;
  }
  $es_events_es_client = ClientBuilder::create()->setHosts($es_hosts)->build();

  return $es_events_es_client;
}
