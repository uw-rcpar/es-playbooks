<?php
/**
 * @file
 * Administration settings callbacks.
 */

use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Elasticsearch\Common\Exceptions\Forbidden403Exception;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Elasticsearch\Common\Exceptions\NoNodesAvailableException;
use Elasticsearch\Common\Exceptions\RuntimeException;

/**
 * Administration settings form.
 */
function es_events_admin_settings($form, &$form_state) {

  $form['switch'] = [
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  ];

  $form['switch']['es_events_enabled'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable logging to Elasticsearch'),
    '#description' => t('This simply provides a way to temporarily disable logging to Elasticsearch.'),
    '#default_value' => variable_get('es_events_enabled', false)
  ];

  $form['server'] = [
    '#type' => 'fieldset',
    '#title' => 'Elasticsearch Server Info',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  ];

  $es_hosts = variable_get('es_events_es_hosts');
  $es_hosts = is_array($es_hosts) ? $es_hosts : [];

  $es_index = variable_get('es_events_es_index', 'events');
  $es_type = variable_get('es_events_es_type', 'events');

  // Run a query to test ES nodes and index.
  try {
    $client = ClientBuilder::create()->setHosts($es_hosts)->build();
    $params = [
      'index' => $es_index,
      'type' => $es_type,
      'body' => [
        'query' => [
          'match' => [
            'eventType' => 'login'
          ]
        ]
      ]
    ];
    $response = $client->search($params);
  } catch (RuntimeException $e) {
    drupal_set_message('ES Error: ' . $e->getMessage(), 'error');
  } catch (NoNodesAvailableException $e) {
    drupal_set_message('No Elasticsearch nodes avaialble. Check your hosts configuration.', 'error');
  } catch (Missing404Exception $e) {
    drupal_set_message('Index does not exist.', 'warning');
  } catch (BadRequest400Exception $e) {
    drupal_set_message('Unable to authenticate to ES.', 'warning');
  } catch (Forbidden403Exception $e) {
    drupal_set_message('ES access insufficient.', 'warning');
  }

  // Build url strings from host arrays.
  $urls = [];
  foreach ($es_hosts as $host) {
    $urls[] = _es_events_build_url($host);
  }

  $form['server']['urls'] = [
    '#type' => 'textarea',
    '#title' => t('Host(s)'),
    '#description' => t('One host per line. i.e. https://user:pass@example.com:9200'),
    '#default_value' => implode("\n", $urls),
    '#required' => TRUE,
    '#rows' => 3
  ];

  $form['data'] = [
    '#type' => 'fieldset',
    '#title' => 'Elasticsearch Data Config'
  ];

  $form['data']['es_events_es_prop_project'] = [
    '#type' => 'textfield',
    '#title' => t('Project'),
    '#description' => t('Sent as the project property with all documents.'),
    '#default_value' => variable_get('es_events_es_prop_project', 'myproject'),
    '#required' => TRUE
  ];

  $form['data']['es_events_es_prop_env'] = [
    '#type' => 'textfield',
    '#title' => t('Environment'),
    '#description' => t('Sent as the env property with all documents.'),
    '#default_value' => variable_get('es_events_es_prop_env', 'dev'),
    '#required' => TRUE
  ];

  $form['data']['es_events_es_index'] = [
    '#type' => 'textfield',
    '#title' => t('Event Index'),
    '#default_value' => $es_index,
    '#required' => TRUE
  ];

  $form['data']['es_events_es_type'] = [
    '#type' => 'textfield',
    '#title' => t('Event Type'),
    '#default_value' => $es_type,
    '#required' => TRUE
  ];

  $form['events'] = [
    '#type' => 'fieldset',
    '#title' => 'Events to Send'
  ];

  $form['events']['es_events_event_CRON'] = [
    '#type' => 'checkbox',
    '#title' => t('Cron events'),
    '#description' => t('Send an event when cron runs.'),
    '#default_value' => variable_get('es_events_event_cron', 1)
  ];

  $form['events']['es_events_event_login'] = [
    '#type' => 'checkbox',
    '#title' => t('Login events'),
    '#description' => t('Send an event when a user logs in.'),
    '#default_value' => variable_get('es_events_event_login', 1)
  ];

  $form['events']['es_events_event_entity'] = [
    '#type' => 'checkbox',
    '#title' => t('Entity events'),
    '#description' => t('Send an event when an entity is changed. (See Entity config.)'),
    '#default_value' => variable_get('es_events_event_entity', 1)
  ];

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function es_events_admin_settings_validate($form, &$form_state) {
  $es_hosts = _es_events_hosts_from_urls($form_state['values']['urls']);
  if ($es_hosts === FALSE) {
    form_set_error('urls', 'Invalid host format detected.');
    return;
  }
  $form_state['values']['es_hosts'] = $es_hosts;
}

/**
 * Implements hook_submit().
 */
function es_events_admin_settings_submit($form, &$form_state) {
  // Set enabled.
  variable_set('es_events_enabled', $form_state['values']['es_events_enabled']);
  variable_set('es_events_es_hosts', $form_state['values']['es_hosts']);

  // Set data config.
  variable_set('es_events_es_index', $form_state['values']['es_events_es_index']);
  variable_set('es_events_es_type', $form_state['values']['es_events_es_type']);
  variable_set('es_events_es_prop_project', $form_state['values']['es_events_es_prop_project']);
  variable_set('es_events_es_prop_env', $form_state['values']['es_events_es_prop_env']);

  // Set event config.
  variable_set('es_events_event_cron', $form_state['values']['es_events_event_cron']);
  variable_set('es_events_event_login', $form_state['values']['es_events_event_login']);
  variable_set('es_events_event_entity', $form_state['values']['es_events_event_entity']);
}

/**
 * Converts raw url input to hosts.
 * Returns host array, or false if any host values failed to parse.
 */
function _es_events_hosts_from_urls($urls) {
  $error = FALSE;
  $urls = explode("\n", trim($urls));
  $es_hosts = [];
  foreach ($urls as $url) {
    if (!empty($url)) {
      $host = parse_url($url);
      if ($host === FALSE) {
        $error = TRUE;
      }
      $es_hosts[] = $host;
    }
  }
  return $error ? FALSE : $es_hosts;
}

/**
 * Helper function to build url.
 */
function _es_events_build_url($parts) {
  return (isset($parts['scheme']) ? "{$parts['scheme']}:" : '') .
    ((isset($parts['user']) || isset($parts['host'])) ? '//' : '') .
    (isset($parts['user']) ? "{$parts['user']}" : '') .
    (isset($parts['pass']) ? ":{$parts['pass']}" : '') .
    (isset($parts['user']) ? '@' : '') .
    (isset($parts['host']) ? "{$parts['host']}" : '') .
    (isset($parts['port']) ? ":{$parts['port']}" : '');
}

/**
 * Entity selection form.
 */
function es_events_admin_entities($form, &$form_state) {
  $bundles_enabled = variable_get('es_events_bundles_enabled');
  $form['#attached']['js'] = [
    drupal_get_path('module', 'es_events') . '/js/entity-form.js'
  ];

  $form['selection'] = [
    '#type' => 'fieldset',
    '#title' => 'Entity Selection',
    '#description' => t('Select the entity/bundles you would like to capture events for.')
  ];

  // Retrieve entity information.
  $entities = entity_get_info();
  $entity_types = array_keys($entities);
  sort($entity_types);

  $form['selection']['types'] = [
    '#tree' => TRUE
  ];

  foreach ($entity_types as $entity_type) {
    $form['selection']['types'][$entity_type] = [
      '#type' => 'fieldset',
      '#title' => $entity_type,
      '#attributes' => ['class' => ['edit-entity']]
    ];

    $key = $entity_type . '._all';
    $form['selection']['types'][$entity_type][$key] = [
      '#type' => 'checkbox',
      '#title' => t('All current and future :et bundles', [':et' => $entity_type]),
      '#attributes' => ['class' => ['select-all-bundles']],
      '#default_value' => isset($bundles_enabled[$key])
    ];

    $bundles_data = field_info_bundles($entity_type);
    $bundles = array_keys($bundles_data);
    sort($bundles);

    foreach ($bundles as $bundle) {
      $key = $entity_type . '.' . $bundle;
      $form['selection']['types'][$entity_type][$key] = [
        '#type' => 'checkbox',
        '#title' => $bundle,
        '#attributes' => ['class' => ['select-bundle', 'select-' . $entity_type . '-bundle']],
        '#default_value' => isset($bundles_enabled[$key])
      ];
    }
  }

  $form['submit'] =[
    '#type' => 'submit',
    '#value' => t('Save Settings')
  ];

  return $form;
}

/**
 * Implements hook_submit().
 */
function es_events_admin_entities_submit($form, &$form_state) {
  $bundles_enabled = [];
  foreach ($form_state['values']['types'] as $type => $bundles) {
    foreach ($bundles as $key => $bundle) {
      if ($bundle === 1) {
        $bundles_enabled[$key] = 1;
      }
    }
  }
  variable_set('es_events_bundles_enabled', $bundles_enabled);
}
