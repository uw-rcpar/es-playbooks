<?php
/**
 * @file
 * ES Events pages
 */

/**
 * ES Events status page.
 */
function es_events_status_page() {
  $token = isset($_GET['token']) ? $_GET['token'] : NULL;
  if (empty($token) || $token !== ES_EVENTS_TOKEN) {
    drupal_not_found();
    exit;
  }

  $data = [];
  $now = time();
  $last = variable_get('cron_last');
  $lapsed = $now - $last;
  $data['cron.last'] = $last;
  $data['cron.lapsed'] = $lapsed;

  // Retrieve entity information.
  $entities = entity_get_info();
  $entity_types = array_keys($entities);
  sort($entity_types);
  //$data['entity.types'] = $entity_types;
  $data['entity.types.count'] = count($entity_types);

  // Retrieve bundle information for each entity.
  foreach ($entity_types as $entity) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $entity);
    $count = $query->count()->execute();
    $data['entity.' . $entity . '.count'] = (int)$count;

    $bundles_data = field_info_bundles($entity);
    $bundles = array_keys($bundles_data);
    sort($bundles);

    //$data['entity.' . $entity_type . '.bundle.types'] = $bundle_types;
    $data['entity.' . $entity . '.bundle.count'] = count($bundles);

    foreach ($bundles as $bundle) {
      try {
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', $entity)
          ->entityCondition('bundle', $bundle);
        $count = $query->count()->execute();
        $data['entity.' . $entity . '.bundle.' . $bundle . '.count'] = (int)$count;
      } catch (\Exception $e) {
        // Skip
      }
    }

    foreach (module_implements('es_events_status_entity') as $module) {
      $function = $module . '_es_events_status_entity';
      $function($entity, $bundles, $data);
    }
  }

  $count = db_query("SELECT COUNT(*) FROM {system} WHERE type = 'module' AND status = 1")->fetchField();
  $data['modules.active'] = (int)$count;

  ksort($data);
  drupal_json_output($data);
}

/**
 * ES Events updates page.
 */
function es_events_updates_page($security_only = FALSE) {
  $token = isset($_GET['token']) ? $_GET['token'] : NULL;
  if (empty($token) || $token !== ES_EVENTS_TOKEN) {
    drupal_not_found();
    exit;
  }

  module_load_include('inc', 'update', 'update.report');
  $available = update_get_available(TRUE);
  $data = update_calculate_project_data($available);

  $updates = [];
  foreach ($data as $key => $d) {
      if ($d['existing_version'] === $d['latest_version']) {
          continue;
      }
      $security_update = is_array($d['security updates']) && count($d['security updates']) > 0;
      if ($security_only && !$security_update) {
        continue;
      }
      $updates[$key] = [
          'name' => $d['name'],
          'existing' => $d['existing_version'],
          'recommended' => $d['recommended'],
          'latest' => $d['latest_version'],
          'security' => $security_update
      ];
  }

  ksort($updates);
  // Set types
  foreach ($updates as $key => $val) {
    if (is_numeric($val)) {
      $str_val = (string)$val;
      if (strpos($str_val, '.') >= 0) {
        $updates[$key] = $val = (float)$val;
      } else {
        $updates[$key] = $val = (int)$val;
      }
    }
  }

  drupal_json_output($updates);
}
