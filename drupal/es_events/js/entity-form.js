(function ($) {
  Drupal.behaviors.eseventsEntityForm = {
    attach: function (context, settings) {
      $selectAllBundles = $('.select-all-bundles');

      $selectAllBundles.each(function(index, element) {
        if ($(element).is(':checked')) {
          setEntityTypeSelections(element);
        }
      });

      $selectAllBundles.change(function() {
        setEntityTypeSelections(this);
      });

      function setEntityTypeSelections(element) {
        var id =  $(element).parent().parent().parent().attr('id');
        var $elements = $('#' + id + ' .select-bundle');
        if ($(element).is(':checked')) {
          $elements.attr('checked', true).attr('disabled', true);
        }
        else {
          $elements.attr('checked', false).removeAttr('disabled');
        }
      }
    }
  }
}(jQuery));
