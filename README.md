## ES Playbooks

This repo contains the [Ansible](http://docs.ansible.com/ansible/) playbooks
used to facilitate the synchronization of logs from Acquia and the transportation
of them to [Elasticsearch](https://www.elastic.co/products/elasticsearch) 
via [Logstash](https://www.elastic.co/products/logstash).

This was developed to be run on [Ubuntu](https://www.ubuntu.com/download/server)
 and modifications may be required to run it on later versions or other distros.
 Package manager and package names being the primary concern with use bewteen
 different versions.
 
## Elastic Cloud Setup

You must enable the following plugins in the configuration section of your
[Elastic Cloud cluster](https://cloud.elastic.co/).

- ingest-geoip
- ingest-user-agent

Once making these changes, it may take a few minutes as the ES instance
will be re-provisioned.
 
## Log Server Setup

Once Ansible is installed, it will do the work of installing dependencies
and configuring the system.

These instructions were taken from the
[Ansible Documentation](http://docs.ansible.com/ansible/intro_installation.html)
so be sure to refer back to them when setting up a new server.

### 1) Install Dependencies

We will manually need to install Ansible and Make.

```
$ sudo apt-get install software-properties-common make
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
```

### 2) Create an SSH Key & Install it on Acquia

The server will use rsync to pull logs from [Acquia](https://www.acquia.com/). 
 For this access you will need to generate an SSH key, and add the public key to 
 an Acquia account which has access to all environments you are syncing logs with.

```
ssh-keygen -t rsa -b 4096
```

This will generate the `~/.ssh/id_rsa.pub` file which will hold the ssh key contents
 to be added over on Acquia.

### 3) Project Config

The base of this repo contains an example file for the project configuration:
 `projects.example`.
 
Copy this file to: `group_vars/all/projects` and update the configuration with
 the environments you wish to transport logs from.
 

### 4) Vault Pass

Elasticsearch credentials are encrypted and stored in this repo using
 Ansible's [Vault](http://docs.ansible.com/ansible/playbooks_vault.html) feature.
 
The encryption key needs to be stored on the system so Ansible can decrypt the
 credentials when it runs.
 
By default, this repo has been setup to look for the key at `~/.vault_pass.txt`,
 however you can move it if desired. Just update the `Makefile` to reflect the
 path of the vault pass file.

The following variables are required to be defined in the vault file.

```
es_host: https://example.com:9256
es_user: elastic
es_pass: 1234567890abcdefghijklm
```

## Run it

There is a `Makefile` setup with targets to run the playbooks.

```
# Run whenever you update the project or vault variables
make

# Run only when starting from scratch.
# This will install the ES pipelines and templates.
make init
```

All you need to do is run `make` from the project directory.
